'use strict';
require('dotenv/config');
const { Pool, Client } = require('pg');

//
const clientz = new Client({
	user: process.env.DBS_USER,
	host: process.env.DBS_HOST,
	database: process.env.DBS_DATABASE,
	password: process.env.DBS_PASSWORD,
	port: process.env.DBS_PORT,
	currentSchema: 'public'
});

function handleDisconnect() {
	// connection = mysql.createConnection(db_config); // Recreate the connection, since
	// the old one cannot be reused.
	clientz.connect(function(err) {
		// The server is either down
		if (err) {
			// or restarting (takes a while sometimes).
			console.log('error when connecting to db:', err);
			setTimeout(handleDisconnect, 1000); // We introduce a delay before attempting to reconnect,
		} // to avoid a hot loop, and to allow our node script to
		// console.log('Database Server Connected!');
	}); // process asynchronous requests in the meantime.
	// If you're also serving http, display a 503 error.
	clientz.on('error', function(err) {
		console.log('db error', err);
		if (err.code === 'PROTOCOL_CONNECTION_LOST') {
			// Connection to the MySQL server is usually
			handleDisconnect(); // lost due to either server restart, or a
		} else {
			// connnection idle timeout (the wait_timeout
			throw err; // server variable configures this)
		}
	});
}

handleDisconnect();

module.exports = clientz;
