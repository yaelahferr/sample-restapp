require('dotenv/config');
const jwt = require('jsonwebtoken');
const accessTokenSecret = process.env.TOKEN_SECRET;

module.exports = {
	authenticate: (req, res, next) => {
		const token = req.cookies.access_token;
		const header = req.headers.token;
		if (token) {
			jwt.verify(token, accessTokenSecret, (err, user) => {
				if (err) {
					return res.sendStatus(403);
				}

				req.user = user;
				next();
			});
		} else if (header) {
			jwt.verify(header, accessTokenSecret, (err, user) => {
				if (err) {
					return res.sendStatus(403);
				}

				req.user = user;
				next();
			});
		} else {
			res.status(401).json({
				message: 'Token is Invalid'
			});
		}
	}
};
