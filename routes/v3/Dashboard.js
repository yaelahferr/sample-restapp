'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const dashboard = require('../../controllers/Dashboard');
const handler = require('../../middleware/jwt');

router.get('/api/dashboard', handler.authenticate, dashboard.get);
router.post('/api/dashboard/add', handler.authenticate, dashboard.add);
router.get('/api/dashboard/last_log', handler.authenticate, dashboard.new_log);
router.post('/api/dashboard/port', handler.authenticate, dashboard.serialconnect);
router.get('/api/dashboard/port', handler.authenticate, dashboard.serialstatus);
router.get('/api/dashboard/portlist', handler.authenticate, dashboard.serialportlist);
router.get('/api/dashboard/trigger', handler.authenticate, dashboard.new_trigger);

module.exports = router;
