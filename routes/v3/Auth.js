'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const auth = require('../../controllers/Auth');

router.post('/api/login', auth.login);

module.exports = router;
