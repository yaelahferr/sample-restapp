'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const city = require('../../controllers/City');
const handler = require('../../middleware/jwt');

router.get('/api/city/list', handler.authenticate, city.get_city);

module.exports = router;
