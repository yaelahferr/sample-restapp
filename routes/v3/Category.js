'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const category = require('../../controllers/Category');
const handler = require('../../middleware/jwt');

router.get('/api/category/list', handler.authenticate, category.get_category);

module.exports = router;
