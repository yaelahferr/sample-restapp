'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const setting = require('../../controllers/Setting');
const handler = require('../../middleware/jwt');

router.get('/api/setting/url', setting.get_ip);
router.post('/api/setting/url', handler.authenticate, setting.set_ip);

module.exports = router;
