'use strict';
const express = require('express');
const router = express.Router();
const report = require('../../controllers/Report');
const handler = require('../../middleware/jwt');

router.get('/generate/report', report.index);
router.get('/download/report', report.download);

module.exports = router;
