'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const users = require('../../controllers/Users');
const handler = require('../../middleware/jwt');

router.get('/api/users/driver', handler.authenticate, users.get_driver);
router.get('/api/users/all', users.get_all);
router.post('/api/users/profile', handler.authenticate, users.update_profil);
router.get('/api/users/profile', handler.authenticate, users.get_profile);
router.post('/api/users/sos', handler.authenticate, users.driver_sos);
router.get('/api/users/sos', handler.authenticate, users.get_sos);

module.exports = router;
