'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const vehicle = require('../../controllers/Vehicle');
const handler = require('../../middleware/jwt');

router.get('/api/vehicle/vin', handler.authenticate, vehicle.get_vin);
router.get('/api/vehicle/list', handler.authenticate, vehicle.get_all);
router.get('/api/vehicle/calibration', handler.authenticate, vehicle.get_calibration);
router.post('/api/vehicle/calibration', handler.authenticate, vehicle.add_calibration);

module.exports = router;
