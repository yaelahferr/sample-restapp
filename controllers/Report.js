'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');
const path = require('path');
const moment = require('moment');
const puppeteer = require('puppeteer');

module.exports.index = async (req, res, next) => {
	let user = req.query.user_id;
	let start = req.query.start;
	let end = req.query.end;
	let mode = req.query.mode;

	if (user && start && end && mode) {
		let query = 'SELECT a.name from com_user a where user_id=$1';

		let value = [ user ];
		await client.query(query, value).then(async (resp) => {
			var result = resp.rows;

			if (mode == 'calibration') {
				var titlez = 'KALIBRASI';
				var query =
					"select a.log_id, c.veh_vin as vin, d.cat_code as jenis_muatan, e.city_code as asal_muatan , a.total_weight as berat_muatan, h.loc_code as location ,(SELECT CASE WHEN a.log_status=true THEN 'Automatic' ELSE 'Manual' END as status), to_char(a.mdb, 'YYYY-MM-DD') as created from log_calibration a inner join vehicle c on a.veh_id = c.veh_id inner join category d on a.cat_id = d.cat_id inner join city e on a.city_id = e.city_id inner join device f on f.device_id = a.device_id INNER JOIN device_unit_location g ON g.device_id = a.device_id INNER JOIN location_weighbridge h ON g.loc_id = h.loc_id WHERE a.mdb between $1 and $2 order by a.mdb DESC";
			} else {
				var titlez = 'MUATAN';

				var query =
					"select a.log_id, c.veh_vin as vin, d.cat_code as jenis_muatan, e.city_code as asal_muatan , a.total_weight as berat_muatan, h.loc_code as location ,(SELECT CASE WHEN a.log_status=true THEN 'Automatic' ELSE 'Manual' END as status), to_char(a.mdb, 'YYYY-MM-DD') as created from log_weigh a inner join vehicle c on a.veh_id = c.veh_id inner join category d on a.cat_id = d.cat_id inner join city e on a.city_id = e.city_id inner join device f on f.device_id = a.device_id INNER JOIN device_unit_location g ON g.device_id = a.device_id INNER JOIN location_weighbridge h ON h.loc_id = g.loc_id WHERE a.mdb between $1 and $2 order by a.mdb DESC";
			}

			await client
				.query(query, [ start + ' 00:00', end + ' 23:59' ])
				.then(async (result2) => {
					result.log = result2.rows;
				})
				.catch(function(err) {
					console.log(err);
				});

			res.render(path.join(__dirname, '../modules/views/invoice/index'), {
				title: titlez,
				operator: resp.rows[0],
				data: result.log,
				location: result.log[0].location,
				start: moment(start).format('DD-MM-YYYY'),
				end: moment(end).format('DD-MM-YYYY')
			});
		});
	} else {
		response.success('', 'Isian tidak lengkap !', res);
	}
};

async function webPageToPDF(user = null, start = null, end = null, mode = null) {
	const browser = await puppeteer.launch({
		headless: true,
		args: [ '--no-sandbox' ]
	});
	const webPage = await browser.newPage();
	const url =
		'http://localhost:5005/generate/report/?user_id=' + user + '&start=' + start + '&end=' + end + '&mode=' + mode;

	await webPage.goto(url, {
		waitUntil: 'networkidle0'
	});

	await webPage
		.pdf({
			printBackground: true,
			displayHeaderFooter: true,
			path: path.join(
				__dirname,
				'../public/report/report-' + mode + '-' + user + '-' + start + '-' + end + '.pdf'
			),
			scale: 2,

			format: 'a4',
			margin: {
				top: '10px',
				bottom: '20px',
				left: '10px',
				right: '20px'
			}
		})
		.then((_) => {
			// console.log('mantap');
		})
		.catch((e) => {
			console.log(e);
		});

	await browser.close();
}

module.exports.download = async (req, res, next) => {
	let user = req.query.user_id;
	let start = req.query.start;
	let end = req.query.end;
	let mode = req.query.mode;
	if (user && start && end && mode) {
		await webPageToPDF(user, start, end, mode);

		var filePath = './public/report/report-' + mode + '-' + user + '-' + start + '-' + end + '.pdf'; // Or format the path using the `id` rest param
		res.download(filePath);
	} else {
		response.success('', 'ID tidak boleh kosong !', res);
	}
};
