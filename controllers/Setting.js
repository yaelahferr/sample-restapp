'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');

module.exports.get_ip = async (req, res, next) => {
	let query = 'select * from apps a order by apps_id desc limit 1';
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.set_ip = async (req, res, next) => {
	let url = req.body.url;
	let url1 = req.body.url1;

	if (url || url1) {
		let query = 'select * from apps a order by apps_id desc limit 1';
		client.query(query, (err, resp) => {
			if (err) {
				console.log(err);
			} else {
				if (resp.rows[0]) {
					if (url) {
						var queryz = 'UPDATE apps SET apps_url=$1 WHERE apps_id=$2  RETURNING *';
						var valuez = [ url, resp.rows[0].apps_id ];
					} else {
						var queryz = 'UPDATE apps SET lamp_url=$1 WHERE apps_id=$2  RETURNING *';
						var valuez = [ url1, resp.rows[0].apps_id ];
					}
				} else {
					var queryz = 'INSERT INTO apps(apps_url, mdb) VALUES($1, CURRENT_TIMESTAMP) RETURNING *';
					var valuez = [ url.toLocaleString() ];
				}

				client.query(queryz, valuez, (err, resp1) => {
					if (err) {
						console.log(err);
					} else {
						response.success(resp1.rows[0], 'Data berhasil diupdate', res);
					}
				});
			}
		});
	} else {
		response.fail('', 'url tidak boleh kosong !', res);
	}
};
