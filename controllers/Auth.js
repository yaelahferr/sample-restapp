'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');
const bcrypt = require('bcrypt');
const helper = require('../helpers/function');
const mailer = require('../helpers/mail');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const accessTokenSecret = process.env.TOKEN_SECRET;
const log_activity = require('../helpers/log_activity');
require('cookie-parser');

const fs = require('fs');
const multer = require('multer');
const path = require('path');
// file configuration logo
const storage = multer.diskStorage({
	destination: path.join(__dirname + './../public/profile'),
	filename: function(req, file, cb) {
		var ext = path.extname(file.originalname);
		const current = moment().utc().format('Y-M-D');

		if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
			return cb(new Error('Hanya file jenis gambar yang diperbolehkan !'));
		}
		cb(null, file.fieldname + '-' + current + '-' + 'logo' + ext);
	}
});

//init upload
const upload = multer({
	storage: storage
});

async function checkAuth($1 = null, $2 = null, req = null, res = null) {
	bcrypt.compare($1, $2.password, function(err, result_hash) {
		if (result_hash == true) {
			if ($2.role_id == 3) {
				const accessToken = jwt.sign(
					{
						user_id: $2.user_id,
						role_id: $2.role_id,
						name: $2.name,
						email: $2.email
					},
					accessTokenSecret,
					{ expiresIn: '23h' }
				);

				let data = {
					user_id: $2.user_id,
					role_id: $2.role_id,
					name: $2.name,
					email: $2.email,
					token: accessToken
				};

				// log_activity
				let ip = log_activity.address(req);
				log_activity.save($2.user_id, ip, 'I', 'Masuk/Login Sistem');

				return res
					.cookie('access_token', accessToken, {
						httpOnly: true,
						secure: false
					})
					.status(200)
					.json({ data });
			} else {
				response.fail('', 'User tidak memiliki hak akses !', res);
			}
		} else {
			response.fail('', 'Email atau password salah !', res);
		}
	});
}

module.exports.login = async (req, res, next) => {
	let email = req.body.email;
	let password = req.body.password;

	if (email && password) {
		let query =
			'SELECT a.*, b.role_id FROM com_user a INNER JOIN com_role b ON b.role_id = a.role_id WHERE a.email=$1 LIMIT 1';
		let value = [ email ];
		client.query(query, value, (err, resp) => {
			if (err) {
				console.log(err);
			} else {
				if (resp.rows[0]) {
					checkAuth(password, resp.rows[0], req, res);
				} else {
					let query =
						'SELECT a.*, b.role_id FROM com_user a INNER JOIN com_role b ON b.role_id = a.role_id WHERE a.username=$1 LIMIT 1';
					let value = [ email ];

					client.query(query, value, (err, resp) => {
						if (err) {
							console.log(err);
						} else {
							if (resp.rows[0]) {
								checkAuth(password, resp.rows[0], req, res);
							} else {
								response.success('', 'Email atau password salah !', res);
							}
						}
					});
				}
			}
		});
	} else {
		response.success('', 'Email atau password tidak boleh kosong !', res);
	}
};
