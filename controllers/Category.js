'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');

module.exports.get_category = async (req, res, next) => {
	let query = 'select a.cat_id , a.cat_code  from category a order by a.cat_code ASC';
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};
