'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');
const clientz = require('../db/pgs');
const SerialPort = require('serialport');
const Delimiter = require('@serialport/parser-delimiter');
var shell = require('shelljs');
const axios = require('axios');
require('dotenv/config');
const fetch = require('isomorphic-fetch');
const log_activity = require('../helpers/log_activity');

const { execFile } = require('child_process');

var serialDelimiter = 'KG';
var serialWeighPort;
var serialWeighParser;
var serialWeighValue = 0;
var serialWeighValueOld = 0;
var serialWeighIsWaiting = true;
var serialUpCounter = 0;
var serialDownCounter = 0;
const serialWeighThd = 255;
var apiUrl = 'http://' + process.env.APP_URL + ':' + process.env.APP_PORT + '/api/setting/url';

async function getJson(url) {
	let response = await fetch(url);
	let data = await response.json();
	return data.result[0].lamp_url;
}

var lampConfig = { name: 'red', change: false };

function serialUpdateData(data) {
	serialWeighValue = parseInt(data);
	console.log(serialWeighValue);
	if (lampConfig.name == 'red' && serialWeighValue > serialWeighValueOld && serialWeighValue > serialWeighThd) {
		serialUpCounter++;
		serialDownCounter = 0;
		if (serialUpCounter > 5) {
			// console.log('Truck naik');
			lampConfig.name = 'yellow';
			lampConfig.change = true;
			serialWeighIsWaiting = false;
			serialUpCounter = 0;
		}
	} else if (
		lampConfig.name == 'green' &&
		(serialWeighValue < serialWeighValueOld || serialWeighValue == 0) &&
		serialWeighValue < 10
	) {
		serialDownCounter++;
		serialUpCounter = 0;
		if (serialDownCounter > 5) {
			// console.log('Truck turun');
			lampConfig.name = 'red';
			lampConfig.change = true;
			serialWeighIsWaiting = false;
			serialDownCounter = 0;
		}
	}
	serialWeighValueOld = serialWeighValue;
	if (lampConfig.change) {
		getJson(apiUrl).then((key) => {
			const response = axios.get('http://' + key + '/light/' + lampConfig.name);
			lampConfig.change = false;
		});
	}
}

module.exports.get = async (req, res, next) => {
	let code = req.query.code;
	let combination = code.substring(0, code.length - 17);
	let device = code.substring(code.length - 16);

	if (code) {
		let query =
			"SELECT a.assignment_id ,c.veh_id ,d.cat_id , e.city_id , c.veh_vin as vin, d.cat_code as jenis_muatan, e.city_code as asal_muatan, (SELECT CASE WHEN a.card_type > 0 THEN 'log_weight' WHEN a.card_type = '0' then 'log_calibration' ELSE 'error' END as type) FROM assignment a inner join vehicle c on a.veh_id = c.veh_id inner join category d on a.cat_id = d.cat_id inner join city e on a.city_id = e.city_id where a.combination =$1";
		let value = [ combination ];
		clientz.query(query, value, (err, resp) => {
			if (err) {
				console.log(err);
			} else {
				if (resp.rows[0]) {
					// get
					var result = resp.rows[0];
					// console.log(result);
					result.total_vehicle = serialWeighValue;

					console.log(serialWeighValue + 'iki cok');

					let queryzx = 'select * from device a where a.uuid=$1';
					let valuezx = [ device ];
					client
						.query(queryzx, valuezx)
						.then(async (result5) => {
							result.device = result5.rows[0].device_id;

							if (resp.rows[0].type == 'log_calibration') {
								let queryz =
									'INSERT INTO log_calibration(veh_id, cat_id, city_id, total_weight ,  device_id, log_status, mdb) VALUES($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP) RETURNING *';
								let valuez = [
									resp.rows[0].veh_id,
									resp.rows[0].cat_id,
									resp.rows[0].city_id,
									serialWeighValue,
									result.device,
									'true'
								];

								console.log(valuez);

								client
									.query(queryz, valuez)
									.then(async (result2) => {
										await log_activity.trigger_exec(
											resp.rows[0].veh_id,
											resp.rows[0].cat_id,
											resp.rows[0].city_id,
											'0',
											serialWeighValue,
											'0'
										);

										lampConfig.name = 'green';
										lampConfig.change = true;
										serialWeighIsWaiting = true;
										response.success(resp.rows, 'Data Ditemukan', res);
									})
									.catch(function(err) {
										console.log(err);
									});
							} else {
								client
									.query(`select * from log_calibration where veh_id =$1 `, [ resp.rows[0].veh_id ])
									.then(async (result9) => {
										result.total_vehicle = parseInt(result9.rows[0].total_weight);
										result.total_weight = serialWeighValue - result.total_vehicle;
										result.total_timbang = serialWeighValue;
										let queryz =
											'INSERT INTO log_weigh( veh_id, cat_id, city_id, total_weight, device_id, log_status, mdb) VALUES($1, $2, $3, $4, $5, $6,  CURRENT_TIMESTAMP) RETURNING *';
										let valuez = [
											resp.rows[0].veh_id,
											resp.rows[0].cat_id,
											resp.rows[0].city_id,
											result.total_weight,
											result.device,
											'true'
										];

										client
											.query(queryz, valuez)
											.then(async (result2) => {
												await log_activity.trigger_exec(
													resp.rows[0].veh_id,
													resp.rows[0].cat_id,
													resp.rows[0].city_id,
													result.total_timbang,
													result.total_vehicle,
													result.total_weight
												);

												lampConfig.name = 'green';
												lampConfig.change = true;
												serialWeighIsWaiting = true;
												clientz
													.query(queryz, valuez)
													.then(async (result3) => {
														let queryzz =
															'UPDATE public.log_weigh SET log_sent=$1 WHERE log_id=$2 RETURNING *';
														let valuezz = [ 'true', result2.rows[0].log_id ];
														client
															.query(queryzz, valuezz)
															.then(async (result4) => {
																// console.log('update log_sent');
															})
															.catch(function(err) {
																console.log(err);
															});
														//insert counting total
														clientz
															.query(
																'INSERT INTO counting_weigh(log_id, count_total, mdb) VALUES($1, $2, CURRENT_TIMESTAMP) RETURNING *',
																[ result2.rows[0].log_id, result.total_weight ]
															)
															.then(async (result5) => {
																// console.log('ok');
															})
															.catch(function(err) {
																console.log(err);
															});
													})
													.catch(function(err) {
														console.log(err);
													});
											})
											.catch(function(err) {
												console.log(err);
											});

										response.success(resp.rows, 'Data Ditemukan', res);
									})
									.catch(function(err) {
										response.fail('', 'Calibration Data Not Found', res);
									});
							}
						})
						.catch(function(err) {
							console.log(err);
						});
				} else {
					response.fail('', 'Data Tidak Ditemukan', res);
				}
			}
		});
	} else {
		response.fail('', 'ID tidak boleh kosong !', res);
	}
};

module.exports.add = async (req, res, next) => {
	let query =
		'INSERT INTO log_weigh( veh_id, cat_id, city_id, total_weight, device_id, log_status, mdb) VALUES($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP) RETURNING *';
	let value = [
		req.body.veh_id,
		req.body.cat_id,
		req.body.city_id,
		req.body.total_weight,
		req.body.device_id,
		req.body.log_status
	];

	client.query(query, value, (err, resp) => {
		if (err) {
			console.log(err);
			response.fail('', 'Error Add Log Weigh', res);
		} else {
			if (resp.rows[0]) {
				var result = resp.rows[0];

				clientz
					.query(query, value)
					.then(async (result3) => {
						let queryzz = 'UPDATE public.log_weigh SET log_sent=$1 WHERE log_id=$2 RETURNING *';
						let valuezz = [ 'true', resp.rows[0].log_id ];
						client
							.query(queryzz, valuezz)
							.then(async (result4) => {
								// console.log('update log_sent');
							})
							.catch(function(err) {
								console.log(err);
							});
						//insert counting total
						clientz
							.query(
								'INSERT INTO counting_weigh(log_id, count_total, mdb) VALUES($1, $2, CURRENT_TIMESTAMP) RETURNING *',
								[ result3.rows[0].log_id, req.body.total_weight ]
							)
							.then(async (result5) => {
								// console.log('ok');
							})
							.catch(function(err) {
								console.log(err);
							});
					})
					.catch(function(err) {
						console.log(err);
					});

				client
					.query(`select a.veh_id, a.veh_vin from vehicle a where a.veh_id=$1`, [ resp.rows[0].veh_id ])
					.then(async (result2) => {
						result.vin = result2.rows[0].veh_vin;
						await client
							.query(`select a.cat_id, a.cat_name from category a where a.cat_id=$1`, [
								resp.rows[0].cat_id
							])
							.then(async (result2) => {
								result.jenis_muatan = result2.rows[0].cat_name;
							})
							.catch(function(err) {
								console.log(err);
							});

						await client
							.query(`select a.city_id, a.city_name from city a where a.city_id=$1`, [
								resp.rows[0].city_id
							])
							.then(async (result2) => {
								result.asal_muatan = result2.rows[0].city_name;
							})
							.catch(function(err) {
								console.log(err);
							});

						response.success(result, 'Success Add Log Weigh', res);
					})
					.catch(function(err) {
						response.fail('', 'Error Add Log Weigh', res);
					});
			} else {
				response.fail('', 'Error Add Log Weigh', res);
			}
		}
	});
};

module.exports.new_log = async (req, res, next) => {
	let query =
		"SELECT a.log_id  , c.veh_vin as vin, d.cat_code as jenis_muatan , e.city_code as asal_muatan, a.total_weight as berat_muatan, (SELECT CASE WHEN a.log_status=true THEN 'Automatic' ELSE 'Manual' END as status), to_char(a.mdb, 'YYYY-MM-DD') as created  FROM log_weigh a inner join vehicle c on a.veh_id = c.veh_id inner join category d on a.cat_id = d.cat_id inner join city e on a.city_id = e.city_id order by a.mdb desc limit 10";
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.new_trigger = async (req, res, next) => {
	let query =
		'select a.trigger_id, b.veh_vin as vin, c.cat_code as jenis_muatan, d.city_code as asal_muatan , a.total_vehicle, a.total_timbang, a.total_weight from trigger_exec a join vehicle b on a.veh_id = b.veh_id join category c on a.cat_id = c.cat_id join city d on d.city_id = a.city_id order by a.trigger_id DESC limit 1';
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.serialconnect = async (req, res, next) => {
	if (serialWeighPort && serialWeighPort.isOpen) {
		var msg = 'Device sudah terhubung dengan: ' + serialWeighPort.path;
		response.failed(msg, res);
	} else {
		var baudrate = 9600;
		if (req.body.baudrate) baudrate = parseInt(req.body.baudrate);
		serialWeighPort = new SerialPort(req.body.port, { baudRate: baudrate }, function(err) {
			if (err) {
				response.failed('Gagal menghubungkan device', res);
			} else {
				response.success('', 'Device terhubung', res);
			}
		});
		serialWeighPort.on('close', () => {
			console.log('Koneksi dengan device terputus');
			serialWeighValue = -1;
		});
		serialWeighParser = serialWeighPort.pipe(new Delimiter({ delimiter: serialDelimiter }));
		serialWeighParser.on('data', serialUpdateData);
	}
};

module.exports.serialstatus = async (req, res, next) => {
	if (serialWeighPort && serialWeighPort.isOpen) {
		response.success('1', 'Device terhubung', res);
	} else {
		response.success('0', 'Device tidak terhubung', res);
	}
};

module.exports.serialportlist = async (req, res, next) => {
	SerialPort.list().then(
		(ports) => {
			response.success(ports, 'Device terhubung', res);
		},
		(err) => {
			response.failed(err, res);
		}
	);
};
