'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');

module.exports.get_vin = async (req, res, next) => {
	let query = 'select a.veh_id , a.veh_vin from vehicle a order by veh_vin ASC';
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.get_all = async (req, res, next) => {
	var start = req.query.start;
	var end = req.query.end;

	var query =
		"select a.log_id, c.veh_vin as vin, d.cat_code as jenis_muatan, e.city_code as asal_muatan , a.total_weight as berat_muatan, (SELECT CASE WHEN a.log_status=true THEN 'Automatic' ELSE 'Manual' END as status), to_char(a.mdb, 'YYYY-MM-DD') as created from log_weigh a inner join vehicle c on a.veh_id = c.veh_id inner join category d on a.cat_id = d.cat_id inner join city e on a.city_id = e.city_id ";
	if (start && end) {
		var query = query + ' where (a.mdb BETWEEN $1 AND $2)  order by a.mdb DESC';
		var value = [ start, end ];
	} else {
		var query = query + ' order by a.mdb DESC';
		var value = [];
	}

	client.query(query, value, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.get_calibration = async (req, res, next) => {
	var start = req.query.start;
	var end = req.query.end;

	var query =
		"select a.log_id, c.veh_vin as vin, d.cat_code as jenis_muatan, e.city_code as asal_muatan , a.total_weight as berat_muatan, (SELECT CASE WHEN a.log_status=true THEN 'Automatic' ELSE 'Manual' END as status), to_char(a.mdb, 'YYYY-MM-DD') as created from log_calibration a inner join vehicle c on a.veh_id = c.veh_id inner join category d on a.cat_id = d.cat_id inner join city e on a.city_id = e.city_id ";
	if (start && end) {
		var query = query + ' where (a.mdb BETWEEN $1 AND $2)  order by a.mdb DESC';
		var value = [ start, end ];
	} else {
		var query = query + ' order by a.mdb DESC';
		var value = [];
	}

	client.query(query, value, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.add_calibration = async (req, res, next) => {
	let query =
		'INSERT INTO log_calibration( veh_id, cat_id, city_id, total_weight, loc_id, device_id, log_status, mdb) VALUES($1, $2, $3, $4, $5, $6, $7, CURRENT_TIMESTAMP) RETURNING *';
	let value = [
		req.body.veh_id,
		req.body.cat_id,
		req.body.city_id,
		req.body.total_weight,
		req.body.loc_id,
		req.body.device_id,
		req.body.log_status
	];

	client.query(query, value, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Success Add Log Calibration', res);
			} else {
				response.success('', 'Error Add Log Calibration', res);
			}
		}
	});
};
