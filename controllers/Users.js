'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const clienz = require('../db/pgs');
const response = require('../res');
const bcrypt = require('bcrypt');
const log_activity = require('../helpers/log_activity');
const TelegramBot = require('node-telegram-bot-api');
const telegram_token = process.env.TELEGRAM_TOKEN;
const telegram_chat = process.env.TELEGRAM_ID_CHAT;
const bot = new TelegramBot(telegram_token);
require('dotenv/config');

module.exports.get_driver = async (req, res, next) => {
	let query =
		"select a.user_id, a.name from com_user a inner join com_role b  on a.role_id = b.role_id where b.role_nm ='driver' order by a.name ASC";
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};

module.exports.get_all = async (req, res, next) => {
	var uuid = req.query.uuid;
	await client.query('select a.veh_id , a.veh_vin from vehicle a order by veh_vin ASC').then(async (resp) => {
		if (uuid) {
			await client
				.query(
					`select a.device_id, a.uuid, c.loc_code as site from device a INNER JOIN device_unit_location b ON b.device_id = a.device_id INNER JOIN location_weighbridge c ON b.loc_id = c.loc_id  where a.uuid=$1 order by a.device_id ASC limit 1`,
					[ uuid ]
				)
				.then(async (result2) => {
					// result.vin = result2.rows;
					response.success(result2.rows, 'Data Ditemukan', res);
				})
				.catch(function(err) {
					console.log(err);
				});
		} else {
			var result = resp.rows;

			await client
				.query(`select a.cat_id , a.cat_code  from category a order by a.cat_code ASC`)
				.then(async (result2) => {
					result.category = result2.rows;
				})
				.catch(function(err) {
					console.log(err);
				});

			await client
				.query(`select a.city_id , a.city_code  from city a order by a.city_code ASC`)
				.then(async (result2) => {
					result.city = result2.rows;
				})
				.catch(function(err) {
					console.log(err);
				});

			await client
				.query(
					`select a.device_id , a.uuid, c.loc_code as site from device a INNER JOIN device_unit_location b ON b.device_id = a.device_id INNER JOIN location_weighbridge c ON b.loc_id = c.loc_id order by device_id ASC`
				)
				.then(async (result2) => {
					result.device = result2.rows;
				})
				.catch(function(err) {
					console.log(err);
				});

			var rez = {
				code: 200,
				status: 'success',
				result: {
					vin: result,
					cat: result.category,
					city: result.city,
					device: result.device
				},
				message: ''
			};
			res.json(rez);
			res.end();
		}
	});
};

module.exports.get_profile = async (req, res, next) => {
	let user_id = req.query.user_id;
	if (user_id) {
		let query =
			'SELECT a.user_id, b.role_id, a.email , a.password FROM com_user a INNER JOIN com_role b ON b.role_id = a.role_id WHERE a.user_id=$1 LIMIT 1';
		let value = [ user_id ];
		client.query(query, value, (err, resp) => {
			if (err) {
				console.log(err);
			} else {
				if (resp.rows[0]) {
					//log activity
					let ip = log_activity.address(req);
					log_activity.save(
						resp.rows[0]['user_id'],
						ip,
						'R',
						'Melihat Data : <b>Profile </b> di Fitur Profile'
					);

					response.success(resp.rows[0], 'Data Ditemukan', res);
				} else {
					response.success('', 'Data Tidak Ditemukan', res);
				}
			}
		});
	} else {
		response.success('', 'ID tidak boleh kosong !', res);
	}
};

module.exports.update_profil = async (req, res, next) => {
	let user_id = req.body.user_id;
	let password = req.body.password;
	let email = req.body.email;

	if (user_id && password && email) {
		let query = 'UPDATE public.com_user SET password=$1 WHERE user_id=$2 RETURNING *';
		let value = [ bcrypt.hashSync(password, bcrypt.genSaltSync()), user_id ];

		client.query(query, value, (err, resp) => {
			if (err) {
				console.log(err);
			} else {
				if (resp.rows[0]) {
					//log activity
					let ip = log_activity.address(req);
					log_activity.save(
						resp.rows[0]['user_id'],
						ip,
						'U',
						'Melakukan perubahan data <b>Profile</b> di Fitur Profile'
					);

					response.success(resp.rows[0], 'Update Profil Success', res);
				} else {
					response.success('', 'Update Profil Gagal', res);
				}
			}
		});
	} else {
		response.success('', 'Data tidak lengkap !', res);
	}
};

module.exports.driver_sos = async (req, res, next) => {
	let device = req.body.uuid;
	var status = req.body.status;

	if (device && status) {
		let queryz =
			'SELECT a.device_id, a.device_name, a.uuid, c.loc_code as site FROM device a INNER JOIN device_unit_location b ON b.device_id = a.device_id INNER JOIN location_weighbridge c ON b.loc_id = c.loc_id WHERE a.uuid=$1 LIMIT 1';
		let valuez = [ device ];

		client.query(queryz, valuez, (err, resp1) => {
			if (err) {
				console.log(err);
			} else {
				if (resp1.rows[0]) {
					if (req.body.status == 'on') {
						var query =
							'INSERT INTO log_emergency(device_id, status, mdb) VALUES($1, $2, CURRENT_TIMESTAMP) RETURNING *';
						var status = [ resp1.rows[0].device_id, '0' ];
						var message =
							'Hello, Your Driver in an emergency on location ' +
							resp1.rows[0].site +
							'  Date : ' +
							new Date().toLocaleString() +
							'. Please help immediately.';
					} else if (req.body.status == 'off') {
						var query = 'UPDATE log_emergency SET status=$2 WHERE device_id=$1  RETURNING *';
						var status = [ resp1.rows[0].device_id, '2' ];
						var message =
							'Hello, Your Driver on location ' +
							resp1.rows[0].site +
							' has canceled the emergency. Date : ' +
							new Date().toLocaleString() +
							'. Thanks You.';
					} else if (req.body.status == 'process') {
						var query = 'UPDATE log_emergency SET status=$2 WHERE device_id=$1  RETURNING *';
						var status = [ resp1.rows[0].device_id, '1' ];
						var message =
							'Hello, Technician is on the way on location ' +
							resp1.rows[0].site +
							' Please wait a moment. Date : ' +
							new Date().toLocaleString() +
							'. Thanks You.';
					}
					clienz.query(query, status, (err, resp) => {
						if (err) {
							console.log(err);
						} else {
							if (resp.rows[0]) {
								bot.sendMessage(telegram_chat, message);

								response.success(resp.rows[0], 'Add SOS Success', res);
							} else {
								response.success('', 'Add SOS Gagal', res);
							}
						}
					});
				}
			}
		});
	} else {
		response.success('', 'ID tidak boleh kosong !', res);
	}
};

module.exports.get_sos = async (req, res, next) => {
	let device = req.query.uuid;

	if (device) {
		let query =
			'select a.log_id, a.device_id, a.status, a.mdb from log_emergency a  INNER JOIN device b on a.device_id = b.device_id where b.uuid=$1 order by mdb desc limit 1';
		let values = [ device ];
		client.query(query, values, (err, resp) => {
			if (err) {
				console.log(err);
			} else {
				if (resp.rows[0]) {
					response.success(resp.rows[0], 'Data ditemukan', res);
				} else {
					response.success('', 'Data tidak ditemukan', res);
				}
			}
		});
	} else {
		response.success('', 'ID tidak boleh kosong !', res);
	}
};
