'use strict';
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const response = require('../res');

module.exports.get_city = async (req, res, next) => {
	let query = 'select a.city_id , a.city_code  from city a order by a.city_code ASC';
	client.query(query, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				response.success(resp.rows, 'Data Ditemukan', res);
			} else {
				response.success('', 'Data Tidak Ditemukan', res);
			}
		}
	});
};
