-- public.apps definition

-- Drop table

-- DROP TABLE public.apps;

CREATE TABLE public.apps (
	apps_id serial4 NOT NULL,
	apps_url text NULL,
	mdb timestamp NULL
);

-- public.category definition

-- Drop table

-- DROP TABLE public.category;

CREATE TABLE public.category (
	cat_id serial4 NOT NULL,
	cat_name varchar(255) NOT NULL,
	mdb timestamp NULL
);


-- public.category foreign keys

-- public.city definition

-- Drop table

-- DROP TABLE public.city;

CREATE TABLE public.city (
	city_id serial4 NOT NULL,
	city_name varchar(255) NOT NULL,
	mdb timestamp NULL
);


-- public.city foreign keys

-- public.com_role definition

-- Drop table

-- DROP TABLE public.com_role;

CREATE TABLE public.com_role (
	role_id serial4 NOT NULL,
	role_nm varchar(50) NOT NULL,
	role_desc varchar(50) NULL,
	mdb timestamp NULL,
	CONSTRAINT com_role_pkey PRIMARY KEY (role_id)
);

-- Permissions

ALTER TABLE public.com_role OWNER TO postgres;

-- public.com_user definition

-- Drop table

-- DROP TABLE public.com_user;

CREATE TABLE public.com_user (
	user_id serial4 NOT NULL,
	role_id int4 NOT NULL,
	"name" varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	username varchar(100) NOT NULL,
	"password" varchar(100) NOT NULL,
	user_sts int4 NULL DEFAULT 0,
	file_photo text NULL,
	mdb timestamp NULL,
	CONSTRAINT com_user_pkey PRIMARY KEY (user_id)
);

-- Permissions

ALTER TABLE public.com_user OWNER TO postgres;

-- public.device definition

-- Drop table

-- DROP TABLE public.device;

CREATE TABLE public.device (
	device_id serial4 NOT NULL,
	device_name varchar(255) NOT NULL,
	uuid varchar(255) NOT NULL,
	mdb varchar NULL
);

-- Permissions

ALTER TABLE public.device OWNER TO postgres;
GRANT ALL ON TABLE public.device TO postgres;

-- public.location_weighbridge definition

-- Drop table

-- DROP TABLE public.location_weighbridge;

CREATE TABLE public.location_weighbridge (
	loc_id serial4 NOT NULL,
	loc_name varchar(255) NOT NULL,
	mdb timestamp NULL
);

-- Permissions

ALTER TABLE public.location_weighbridge OWNER TO postgres;
GRANT ALL ON TABLE public.location_weighbridge TO postgres;

-- public.log_activity definition

-- Drop table

-- DROP TABLE public.log_activity;

CREATE TABLE public.log_activity (
	log_id serial4 NOT NULL,
	user_id int4 NULL,
	"action" varchar(5) NULL,
	description text NULL,
	ip varchar(100) NULL,
	mdb timestamp NULL
);

-- Permissions

ALTER TABLE public.log_activity OWNER TO postgres;

-- public.log_calibration definition

-- Drop table

-- DROP TABLE public.log_calibration;

CREATE TABLE public.log_calibration (
	log_id serial4 NOT NULL,
	user_id int4 NOT NULL,
	veh_id int4 NOT NULL,
	cat_id int4 NOT NULL,
	city_id int4 NOT NULL,
	total_weight varchar(255) NULL,
	loc_id int4 NULL,
	device_id int4 NULL,
	log_status bool NULL DEFAULT true,
	mdb timestamp NULL,
	log_sent bool NULL DEFAULT false
);

-- Permissions

ALTER TABLE public.log_calibration OWNER TO postgres;
GRANT ALL ON TABLE public.log_calibration TO postgres;

-- public.log_emergency definition

-- Drop table

-- DROP TABLE public.log_emergency;

CREATE TABLE public.log_emergency (
	log_id serial4 NOT NULL,
	user_id int4 NOT NULL,
	status varchar(5) NULL DEFAULT 0,
	mdb timestamp NULL
);

-- Permissions

ALTER TABLE public.log_emergency OWNER TO postgres;
GRANT ALL ON TABLE public.log_emergency TO postgres;

-- public.log_weigh definition

-- Drop table

-- DROP TABLE public.log_weigh;

CREATE TABLE public.log_weigh (
	log_id serial4 NOT NULL,
	user_id int4 NOT NULL,
	veh_id int4 NOT NULL,
	cat_id int4 NOT NULL,
	city_id int4 NOT NULL,
	total_weight varchar(255) NULL,
	loc_id int4 NULL,
	device_id int4 NULL,
	log_status bool NULL DEFAULT true,
	mdb timestamp NULL,
	log_sent bool NULL DEFAULT false
);

-- Permissions

ALTER TABLE public.log_weigh OWNER TO postgres;
GRANT ALL ON TABLE public.log_weigh TO postgres;

-- public.vehicle definition

-- Drop table

-- DROP TABLE public.vehicle;

CREATE TABLE public.vehicle (
	veh_id serial4 NOT NULL,
	veh_name varchar(255) NOT NULL,
	veh_vin varchar(255) NULL,
	mdb timestamp NULL
);

-- Permissions

ALTER TABLE public.vehicle OWNER TO postgres;
GRANT ALL ON TABLE public.vehicle TO postgres;