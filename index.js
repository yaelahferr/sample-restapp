const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const engine = require('ejs-blocks');
require('dotenv/config');
const localtunnel = require('localtunnel');

// view engine setup
app.set('views', path.join(__dirname, 'modules'));
app.engine('ejs', engine);
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

app.use('/generate', express.static('public'));
app.use('/generate/report', express.static('public'));

const subdomain = process.env.BASE_LOCAL;
const port = process.env.APP_PORT;
app.use(cookieParser());

var response = require('./res');
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/v3/assets/', express.static('public'));

var corsOptions = {
	origin: '*',
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
};

app.use(cors(corsOptions));

const auth = require('./routes/v3/Auth');
const dashboard = require('./routes/v3/Dashboard');
const users = require('./routes/v3/Users');
const vehicle = require('./routes/v3/Vehicle');
const category = require('./routes/v3/Category');
const city = require('./routes/v3/City');
const setting = require('./routes/v3/Setting');
const report = require('./routes/v3/Report');

app.use(auth);
app.use(dashboard);
app.use(users);
app.use(vehicle);
app.use(category);
app.use(city);
app.use(setting);
app.use(report);

const https = require('https');
const fs = require('fs');

var options = {
	key: fs.readFileSync('key.pem'),
	cert: fs.readFileSync('cert.pem')
};

var server = https.createServer(options, app);

server.timeout = 1200000;

/**
 * Listen on provided port, on all network interfaces.
 */

app.listen(port, () => {
	 (async () => {
	 	const tunnel = await localtunnel({ port: port, subdomain: subdomain });

	 	tunnel.url;
	 	console.log(tunnel.url);

	 	// tunnel.on('close', () => {
	 	// 	// tunnels are closed
	 	// });
	 })();
	console.log(`Example app listening at http://localhost:${port}`);
});
