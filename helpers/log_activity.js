'use strict';
require('dotenv/config');
const express = require('express');
const router = express.Router();
const client = require('../db/pg');
const requestIp = require('request-ip');

//get address headers
exports.address = function($1 = null) {
	let ip = requestIp.getClientIp($1);
	return ip;
};

//save log
exports.save = function($1 = null, $2 = null, $3 = null, $4 = null) {
	let query =
		'INSERT INTO log_activity(user_id, ip, mdb, action, description) VALUES($1, $2, CURRENT_TIMESTAMP, $3, $4) RETURNING *';
	let value = [ $1, $2, $3, $4 ];

	client.query(query, value, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				// console.log('log success');
			} else {
				// console.log('log error');
			}
		}
	});
};

exports.trigger_exec = function($1 = null, $2 = null, $3 = null, $4 = null, $5 = null, $6 = null) {
	let query =
		'INSERT INTO trigger_exec(veh_id, cat_id, city_id, total_timbang, total_vehicle, total_weight, mdb) VALUES($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP) RETURNING *';
	let value = [ $1, $2, $3, $4, $5, $6 ];

	client.query(query, value, (err, resp) => {
		if (err) {
			console.log(err);
		} else {
			if (resp.rows[0]) {
				// console.log('log success');
			} else {
				// console.log('log error');
			}
		}
	});
};
