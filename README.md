
# Sample Rest
----

### Description
Sample Rest


### Requirement
- NPM
- NodeJs
- Text Editor (VS code, Sublime, Atom, etc)

### How To Run Backend
1. cd restapps
2. cp -r env-example .env
3. change environment (.env) 
4. db-migrate up initialize (migrate automatic database postgres)
5. nodemon or node index.js
