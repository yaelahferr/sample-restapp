'use strict';

exports.success = function(values = [], message = '', res) {
	var data = {
		code: 200,
		status: 'success',
		result: values,
		message: message
	};
	res.json(data);
	res.end();
};

exports.fail = function(values = [], message = '', res) {
	var data = {
		code: 200,
		status: 'failed',
		result: values,
		message: message
	};
	res.json(data);
	res.end();
};

exports.failed = function(message, res) {
	var data = {
		code: 500,
		status: 'failed',
		messgae: message
	};
	res.json(data);
	res.end();
};
